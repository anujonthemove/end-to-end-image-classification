### Image Recognition on Caltech 101 dataset

#### Directory Layout
```
.
├── Readme.md
├── Report_on_Image_Recognition_on_Caltech_101_dataset.pdf
├── requirements.txt
├── Results
│   ├── Common-Results.ipynb
│   ├── Common-Training-Plots.ipynb
│   ├── MobileNetV2
│   │   ├── MobileNetV2-Categorical-Cross-Entropy-Loss-Results.html
│   │   ├── MobileNetV2-Categorical-Cross-Entropy-Loss-Training-Plots.html
│   │   ├── MobileNetV2-Focal-Loss-Results.html
│   │   └── MobileNetV2-Focal-Loss-Training-Plots.html
│   ├── readme.md
│   ├── Resnet50
│   │   ├── ResNet50-Results.html
│   │   └── ResNet50-Training-Plots.html
│   ├── VGG-19
│   │   ├── VGG-19-Categorical-Cross-Entropy-Loss-Results.html
│   │   ├── VGG-19-Categorical-Cross-Entropy-LossTraining-Plots.html
│   │   ├── VGG-19-Focal-Loss-Results.html
│   │   └── VGG-19-Focal-Loss-Training-Plots.html
│   └── Xception
│       ├── Xception-Results.html
│       └── Xception-Training-Plots.html
├── Training-Pipeline
│   ├── pipeline-using-image_dataset_from_directory.ipynb
│   └── pipeline-using-tf.data.Dataset.ipynb
└── Utils
    ├── data-exploration.html
    ├── data-exploration.ipynb
    └── split-folders.ipynb

7 directories, 23 files


```

#### 1. Results Directory
*   Common-Results.ipynb - Jupyter Notebook for plotting results, confusion matrix, classification report
*   Common-Training-Plots.ipynb - Notebook for plotting model training stats
    *   MobileNetV2 - directory containing results for MobileNetV2 model 
    *   Resnet50 - directory containing results for Resnet50 model 
    *   VGG-19 - directory containing results for VGG-19 model 
    *   Xception - directory containing results for Xception model 

#### 2. Training-Pipeline Directory
*   pipeline-using-image_dataset_from_directory.ipynb - complete model training pipeline
*   pipeline-using-tf.data.Dataset.ipynb - complete model training pipeline

#### 3. Utils Directory
* This contains code for:
    * data-exploration.ipynb - codes for exploring data
    * data-exploration.html - html view of the notebook for a quick look
    * split-folders.ipynb - code for splitting data into train, validation, and test sets

#### 4. Report_on_Image_Recognition_on_Caltech_101_dataset.pdf
* This is the report of all the work that has been done in this exercise

