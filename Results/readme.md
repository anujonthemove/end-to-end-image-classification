# Model Experiments

```
models
├── mobilenet
│   ├── categorical_loss
│   │   ├── finetuned_model
│   │   │   ├── cat_finetuned_history_06_06_2021_mobilenet_eps_50_acc_94.pickle
│   │   │   ├── cat_finetuned_model_06_06_2021_mobilenet_eps_50_acc_94.h5
│   │   │   └── cat_finetuned_model_06_06_2021_mobilenet_eps_50_acc_highest.h5
│   │   └── model
│   │       ├── cat_model_06_06_2021_mobilenet_eps_100_acc_highest.h5
│   │       ├── cat_model_06_06_2021_mobilnet_eps_100_acc_85.h5
│   │       └── history_06_06_2021_mobilnet_eps_100_acc_85.pickle
│   └── focal_loss
│       ├── finetuned_model
│       │   ├── finetuned_history_06_06_2021_mobilenet_eps_50_acc_97.pickle
│       │   ├── finetuned_model_06_06_2021_mobilenet_eps_100_acc_97.h5
│       │   ├── finetuned_model_06_06_2021_mobilenet_eps_100_acc_highest.h5
│       │   └── finetuned_model_06_06_2021_mobilenet_eps_50_acc_highest.h5
│       └── model
│           ├── history_06_06_2021_mobilnet_eps_100_acc_94.pickle
│           ├── model_06_06_2021_mobilenet_eps_100_acc_94.h5
│           └── model_06_06_2021_mobilenet_eps_100_acc_highest.h5
├── resnet
│   ├── categorical
│   └── focal
│       ├── finetuned_model
│       │   ├── finetuned_history_06_06_2021_resnet_eps_50_acc_96.pickle
│       │   ├── finetuned_model_06_06_2021_resnet_eps_50_acc_96.h5
│       │   └── finetuned_model_06_06_2021_resnet_eps_50_acc_highest.h5
│       └── model
│           ├── history_06_06_2021_resnet50_eps_100_acc_97.pickle
│           ├── model_06_06_2021_resnet50_eps_100_acc_97.h5
│           └── model_06_06_2021_resnet50_eps_100_acc_highest.h5
├── vgg
│   ├── categorical
│   │   ├── finetuned_model
│   │   │   ├── cat_finetuned_model_06_06_2021_vgg19_eps_50_acc_96.h5
│   │   │   ├── cat_finetuned_model_06_06_2021_vgg19_eps_50_acc_96.pickle
│   │   │   └── cat_finetuned_model_06_06_2021_vgg19_eps_50_acc_highest.h5
│   │   └── model
│   │       ├── cat_model_06_06_2021_vgg19_eps_100_acc_91.h5
│   │       ├── cat_model_06_06_2021_vgg19_eps_100_acc_91.pickle
│   │       └── cat_model_06_06_2021_vgg19_eps_100_acc_highest.h5
│   └── focal
│       ├── finetuned_model
│       │   ├── 06_06_2021_vgg-19_fine_tuned_eps_50_acc_94.h5
│       │   └── history_06_06_2021_vgg_19_finetuned_eps_50.pickle
│       └── model
│           ├── 06_06_2021_VGG_19_EPS_100_ACC_98.h5
│           └── history_06_06_2021_vgg_19_eps_100.pickle
└── xception
    ├── categorical_loss
    └── focal_loss
        ├── finetuned_model
        │   ├── finetuned_history_06_06_2021_resnet_eps_50_acc_96.pickle
        │   ├── finetuned_history_06_06_2021_xception_eps_100_acc_95.pickle
        │   ├── finetuned_model_06_06_2021_xception_eps_100_acc_95.h5
        │   └── finetuned_model_06_06_2021_xception_eps_100_acc_highest.h5
        └── model
            ├── history_06_06_2021_xception_eps_100_acc_95.pickle
            ├── model_06_06_2021_xception_eps_100_acc_95.h5
            └── model_06_06_2021_xception_eps_100_acc_highest.h5

24 directories, 36 files

```